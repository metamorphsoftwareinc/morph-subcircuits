#Blank python file ready for quick code insertion
import os
import csv


def GenerateCSV():
	with open('adm_categories.csv', 'w') as csvfile:
		writer = csv.writer(csvfile)
		for key, value in adms.items():
			writer.writerow([key, value])
		#for key, value in cdict.items():
		#	writer.writerow([key, value])

def Categorize(head, tail):
	head = head[2:]
	category = head[:head.index("\\")]
	adm = tail[:tail.index(".adm")]
	adms[category].append(adm)

adms = {}

for d in os.listdir("."):
	adms[d] = []

n = 0

for root, dirs, files in os.walk("."):
	for f in files:
		if f.endswith(".adm"):
			n = n + 1
			head, tail = os.path.split(os.path.join(root, f))
			Categorize(head, tail)

adms = dict((k, v) for k, v in adms.iteritems() if v)
GenerateCSV()
print "%d subcircuits found." %n
