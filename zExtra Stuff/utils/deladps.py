import os

for root, dirs, files in os.walk(os.getcwd()):
	for f in files:
		if f.endswith(".adp"):
			fullpath = os.path.join(root, f)
			try:
				os.remove(fullpath)
				print "deleted %s" %fullpath
			except OSError:
				pass
				print "could not delete %s" %fullpath