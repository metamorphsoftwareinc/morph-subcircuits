#Blank python file ready for quick code insertion
import os
from lxml import etree as ET

def ConcatBoundingBoxADM(path, element, attribute):
	comp_assembly = ET.parse(path)
	root = comp_assembly.getroot()

	for child in root.iter(element):
		box = child.get(attribute)
		#print box
		if ":" not in box:
			top = box + ",0"
			bottom = box + ",1"
			fullBox = top + ":" + bottom
			print fullBox
			child.set(attribute, fullBox)

	comp_assembly.write(path)

def ConcatBoundingBoxXME(path, element, qualifier, attribute):
	comp_assembly = ET.parse(path)
	root = comp_assembly.getroot()

	for child in root.iter(element):
		if child.get("name") == qualifier:
			print child.get("name")

			box = child.find(attribute)
			if not box.text:
				return
			if ":" not in box.text:
				top = box.text + ",0"
				bottom = box.text + ",1"
				fullBox = top + ":" + bottom
				box.text = box.text.replace(box.text, fullBox)
				print box.text

	comp_assembly.write(path)

	# with open(path, 'rb+') as xme:
	# 	xme.seek(0)
	# 	xme.write(ET.tostring(comp_assembly, encoding='UTF-8', xml_declaration=True))
	# 	xme.truncate


for root, dirs, files in os.walk("."):
	for f in files:
		if f.endswith(".adm"):
			f_path = os.path.abspath(os.path.join(root, f))
			ConcatBoundingBoxADM(f_path, "DomainModel", "BoundingBoxes")
		if f.endswith(".xme"):
			x_path = os.path.abspath(os.path.join(root, f))
			ConcatBoundingBoxXME(x_path, "regnode", "layoutBox", "value")