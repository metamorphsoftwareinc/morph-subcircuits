__author__ = 'Henry'
#
# Testing subcircuits, CT-108

import sys
import os
import os.path
import fnmatch
from lxml import etree
import unittest
import requests
import json
import time
import zipfile
import shutil
import errno
import sys
import unittest.loader

if sys.version <= (2.7):
    raise Exception("Python versions less than 2.7 are unsupported")

server_url = "http://components.metamorphsoftware.com"
#server_url = "http://localhost:3000"

component_cache_dir = 'downloaded_components'

loaded_components = set()

repo_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

debugging = False


def find_acm_file(path):
    for root, dirnames, filenames in os.walk(path):
        matches = fnmatch.filter(filenames, '*.acm')
        if len(matches) > 0:
            return root + "/" + matches[0]
    return None


# get acm XML
def get_acm( compId ):
    try:
        os.makedirs(component_cache_dir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
    # download and unzip if not already done
    dirname = os.path.join(component_cache_dir, compId)
    if compId not in loaded_components:
        headers = {}
        try:
            timestamp = os.stat(dirname).st_mtime
            headers = { "If-Modified-Since": time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(timestamp)) }
        except Exception as e:
            pass
        
        # TODO convert to /getcomponent/acm/
        rest_url = server_url + "/getcomponent/download/" + compId
        r = requests.get(rest_url, headers=headers)
        if r.status_code > 399:
            raise Exception("Could not download component {}: server returned {}".format(compId, r.status_code))
        # print str(r.status_code) + '  ' + repr(headers)
        if r.status_code == 200:
            # print repr(r.headers)
            try:
                os.mkdir(dirname)
            except OSError:
                if not os.path.isdir(dirname):
                    raise
            fname = os.path.join(dirname, compId + ".zip")
            with open(fname, "wb") as f:
                f.write(r.content)
            f.closed
            with zipfile.ZipFile(fname, "r") as z:
                z.extractall(dirname)
            os.remove(fname)
            loaded_components.add(compId)
        else:
            loaded_components.add(compId)
    # find and parse acm file
    acm_fname = find_acm_file(dirname)
    if acm_fname != None:
        return etree.parse(acm_fname)
    else:
        return None
        

# get the current time
def currentTime():
    return time.time()

def find_adm_files(path):
    matches = []
    print path
    for root, dirnames, filenames in os.walk(path):
        for filename in fnmatch.filter(filenames, '*.adm'):
            matches.append(os.path.join(root, filename))

    return matches

###
class TestAllSubcircuits(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        for d in loaded_components:
            shutil.rmtree(os.path.join(component_cache_dir, d))

    pass
###
   
all_design_ids = {}

def test_function_generator(path):
    def check_id_unique(self):
        adm = etree.parse(path)
        id = adm.getroot().attrib['DesignID']
        self.assertTrue(id not in all_design_ids, '%s has a duplicate id %s' % (path, id))
        all_design_ids[id] = path
    
    def check_ids(self):
        adm = etree.parse(path)
        for id_name in ('ID', 'ComponentID'):
            for id in (node.attrib[id_name] for node in adm.findall('//*[@%s]' % id_name)):
                self.assertTrue(' ' not in id, '%s %s contains spaces' % (id_name, id))

    def run_on_all_comp_instance(func):
        adm = etree.parse(path)
        root = adm.getroot()
        componentElementList = [ el for el in root.iter() if el.tag == 'ComponentInstance']

        error_msg = ""
        for component_instance in componentElementList:
            error_msg += func(component_instance)

        return error_msg


    # For each component in the subcircuit, make sure it's available from the production Component Server.
    def components_available(self):
        # Define a function to run on all ComponentInstances
        def test_func(component_instance):
            error_msg = ""

            comp_definition_id = component_instance.attrib['ComponentID']
            acm = get_acm(comp_definition_id)
            if acm is None:
                error_msg += "Component '{0}' with AVMID '{1}' wasn't found in the Component database.\n"\
                    .format(component_instance.attrib['Name'], comp_definition_id)

            return error_msg

        # Run our function on all component instances, and gather the resulting error message ("" if no errors)
        all_error_messages = run_on_all_comp_instance(test_func)

        # Assert that error message is empty. If not, print the error message.
        self.assertTrue( len(all_error_messages) == 0, all_error_messages )


    # For each component in the subcircuit, check to see if it has connectors that match the stated IDinComponentModel
    # that's specified in the ComponentInstance element
    def check_connector_ids(self):
        # Define a function to run on all ComponentInstances
        def test_func(component_instance):
            error_msg = ""

            comp_definition_id = component_instance.attrib['ComponentID']
            comp_instance_name = component_instance.attrib['Name']
            acm = get_acm(comp_definition_id)
            if acm is None:
                # No component found by this ID. Skip it. The other test will find this problem.
                # In this function we only want to check that the Connector IDs match up.
                return error_msg

            acm_connectors = set()
            for ac in acm.findall("./Connector"):
                acm_connectors.add(ac.attrib['ID'])

            for connector_id_in_src_model in component_instance.findall("./ConnectorInstance/[@IDinComponentModel]"):
                if not connector_id_in_src_model.attrib['IDinComponentModel'] in acm_connectors:
                    error_msg += "Expected component '{0}' with AVMID '{1}' to have a Connector with ID '{2}', " \
                                 "but it doesn't have one.\n" \
                                 "The component in the repository probably doesn't match the one used to " \
                                 "build this subcircuit.\n\n"\
                        .format(comp_instance_name, comp_definition_id, connector_id_in_src_model.attrib['IDinComponentModel'])

            return error_msg

        # Run our function on all component instances, and gather the resulting error message ("" if no errors)
        all_error_messages = run_on_all_comp_instance(test_func)

        # Assert that error message is empty. If not, print the error message.
        self.assertTrue( len(all_error_messages) == 0, all_error_messages )


    full_test_list = [check_id_unique,
                        check_ids,
                        components_available,
                        check_connector_ids]
    return full_test_list

###

adm_files = find_adm_files(repo_dir)
print "Found %i ADM files to test" % len(adm_files)

for f in adm_files:
    # print t
    test_name_prefix = 'test_%s' % f[len(repo_dir)+1:]
    tests = test_function_generator(f)
    for test in tests:
        setattr(TestAllSubcircuits, test_name_prefix.replace('\\', '/').replace('/', '.').replace('&','&amp;') + '_' + test.__name__, test)

def run_xmlrunner(tests, output_filename):
    import os.path
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'MetaPyUnit'))
    import xmlrunner
    results = []
    with open(output_filename, "w") as output:
        output.write("<testsuites>")
        for test in tests:
            runner = xmlrunner.XMLTestRunner(output)
            runner.run(test)
        output.write("</testsuites>")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--component-url", help="URL to component server", default=server_url)
    parser.add_argument("--xml", action='store_true')
    parser.add_argument("--verbose", action='store_true')
    args = parser.parse_args()
    server_url = getattr(args, 'component_url', server_url)
    print 'Using component-url %s' % server_url

    testpath = os.path.dirname(os.path.abspath(__file__))
    #tests = unittest.loader.defaultTestLoader.discover(testpath, pattern='*.py')
    tests = unittest.TestLoader().loadTestsFromTestCase(TestAllSubcircuits)

    if args.xml:
        run_xmlrunner(tests, 'tests.xml')
    else:
        runner = unittest.TextTestRunner(verbosity=2 if args.verbose else 1)
        runner.run(tests)
