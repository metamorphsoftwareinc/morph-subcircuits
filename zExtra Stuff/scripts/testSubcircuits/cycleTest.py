__author__ = 'Henry'

# Program to test for cycles in the signal nets of subcircuits.  See CT-168.

'''
ABOUT ADM FILES:

Subcircuits correspond to adm files, a special type of XML file. The wires shown to users in the GME window don't
have a corresponding data entry in the adm file; instead they are inferred from the Connector Compositions.

Here's a simplified view of a component, represented as a ComponentInstance in an adm file:

<ComponentInstance ID="{0ebc8e6a}" Name="R6">
    <ConnectorInstance ConnectorComposition="{6112ab32}-id-6f6b33f6" ID="{0ebc8e6a}-id-9f05f41f" />
    <ConnectorInstance ConnectorComposition="{6112ab34}-id-65433321" ID="{0ebc8e84}-id-0f15348c" />
</ComponentInstance>

Each ConnectorInstance represents a "pin" of the component.  The value of the ConnectorComposition within a
ConnectorInstance represents a space-delimited list of the IDs of the other pins that connect to this one.
So, in this example, the component has two pins, each of which have one connector to some other endpoint.

Those endpoints might be pins in other components.  Alternatively, they might be in a "Connector".  Here's a
simplified example of a connector:

<Connector ConnectorComposition="{8310c7a1}-id-e54b6095" ID="id-03e4efb9" Name="INT2(XM)">
    <Role xmlns:q2="schematic" xsi:type="q2:Pin" ID="id-15da135c" PortMap="" Notes="" />
</Connector>

So, connectors also contain a ConnectorComposition, which again contains a space-delimited list of the IDs of
the other endpoints that connect to this one.  In this example, only one other endpoint is connected to this
connector.

'''

############## import files
import sys
import os
import copy
from lxml import etree


# Class to test the signal nets of a subcircuit
class NetList:

    # initialize module data from the adm file
    def __init__(self, adm_path):
        self.vertices = set()   # The nodes of the graph representing signal nets, corresponding to connection IDs
        self.edges = [] # The edges of the graph representing connections between nodes.
        self.component_id_to_name_map = {}   # Dictionary mapping component instance IDs to component names.
        self.connector_id_to_name_map = {}   # Dictionary mapping connector instance IDs to component or connector names.
        self.netList = [] #  each element is a list representing one unique set of connections (a single wire signal net)

        self.adm = etree.parse(adm_path)     # parse the adm file as an XML file.
        self.root = self.adm.getroot()

        self.componentInstanceList = [ el for el in self.root.iter() if el.tag == 'ComponentInstance']
        for component in self.componentInstanceList:
            self.parse_component( component )

        self.connectorInstanceList = [ el for el in self.root.iter() if el.tag == 'Connector']
        for connector in self.connectorInstanceList:
            self.parse_connector( connector )

        self.normalize_and_sort_edges()
        self.merge_edges_into_netlist()
        # self.show_netlist()

        return

    def parse_component( self, component ):
        if 'ID' in component.attrib:
            componentInstanceId = component.attrib[ 'ID' ]
            # if component isn't already in component_id_to_name_map, add it.
            existingName = self.component_id_to_name_map.get( componentInstanceId )
            if not existingName:
                if 'Name' in component.attrib:
                    compName = component.attrib['Name']
                    self.component_id_to_name_map[ componentInstanceId ] = compName
                else:
                    print "Warning, component {0} has no name!".format( componentInstanceId )
            else:
                print "Warning, duplicate component instance found for " + existingName

            # Get a list of the connector instances for this component
            connectorInstances = [x for x in component if x.tag == 'ConnectorInstance']
            for ci in connectorInstances:
                if 'ID' in ci.attrib:
                    connectorInstanceId = ci.attrib[ 'ID' ]
                    # if connector instance isn't already in connector_id_to_name_map, add it.
                    existingNameFromConnector = self.connector_id_to_name_map.get( connectorInstanceId )
                    if not existingNameFromConnector:
                        self.connector_id_to_name_map[ connectorInstanceId ] = compName
                    else:
                        print "Warning, duplicate connector instance found in component " + compName

                    # Check for a connector composition
                    if 'ConnectorComposition' in ci.attrib:
                        cc = ci.attrib['ConnectorComposition']
                        self.parse_connector_composition( cc, connectorInstanceId )
                else:
                    print "Warning, no ID found in connector instance of " + compName

        else:
            print "Warning, component instance found without an ID"
        return

    def parse_connector_composition( self, cc, connectorInstanceId ):
        self.vertices.add(connectorInstanceId)
        compIdList = cc.split(' ')
        for id in compIdList:
            self.edges.append( (connectorInstanceId, id) )
            self.vertices.add(id)
        return


    def parse_connector( self, connector ):
        if 'ID' in connector.attrib:
            connectorInstanceId = connector.attrib[ 'ID' ]
            # if connector isn't already in connector_id_to_name_map, add it.
            existingName = self.connector_id_to_name_map.get( connectorInstanceId )
            if not existingName:
                if 'Name' in connector.attrib:
                    connectorName = "conn:" + connector.attrib['Name']
                    self.connector_id_to_name_map[ connectorInstanceId ] = connectorName
                else:
                    print "Warning, connector has no name: " + connectorInstanceId
            else:
                print "Warning, duplicate name found for connector " + existingName
            if 'ConnectorComposition' in connector.attrib:
                cc = connector.attrib['ConnectorComposition']
                self.parse_connector_composition( cc, connectorInstanceId )
        else:
            print "Warning, connector instance found without an ID"
        return

    def normalize_and_sort_edges(self):
        cleanEdges = set()
        for edge in self.edges:
            cleanEdge = tuple( sorted( list( edge ) ) )
            cleanEdges.add( cleanEdge )
        self.edges = list( cleanEdges )
        return

    def merge_edges_into_netlist(self):
        done = False
        self.netList = copy.deepcopy( self.edges)
        while not done:
            done = True
            for i, net in enumerate(self.netList):
                netSet = set(net)
                for j, net2 in enumerate(self.netList):
                    if j > i:
                        net2Set = set(net2)
                        commonIds = netSet & net2Set
                        if len(commonIds) > 0:
                            mergedNet = list( netSet | net2Set )
                            self.netList[ i ] = mergedNet
                            del self.netList[j]
                            done = False
                            break

        return

    def show_netlist(self):
        print "Netlist:"
        for net in self.netList:
            print "    " + self.net_to_string( net )

    def net_to_string(self, net):
        partNameToPinCount = {}
        partNames = []

        for id in net:
            compName = self.connector_id_to_name_map.get( id, '?')
            if compName in partNameToPinCount:
                partNameToPinCount[compName] += 1
            else:
                partNameToPinCount[compName] = 1

        for partName in partNameToPinCount:
            pinCount = partNameToPinCount[ partName ]
            if pinCount > 1:
                partNames.append( "{0} ({1} pins)".format(partName, pinCount) )
            else:
                partNames.append( partName )

        partNames = sorted(partNames)
        msg = ', '.join( partNames )
        return msg

    # This is the function that checks connections for cycles,
    # and returns a non-empty error-message string if any cycles are found.

    def check_for_cycles(self):
        msg = ""
        # Each net is a connected graph.  Figure out how many vertices and edges each net has.
        netInfo = []
        for i, net in enumerate( self.netList ):
            info = {"index" : i, "net" : net, "vertex_count" : len(net), "edge_count" : 0 }
            netInfo.append( info )

        # Fill in the missing edge counts
        for edge in self.edges:
            id = edge[0]
            for info in netInfo:
                if id in info["net"]:
                    info["edge_count"] += 1

        # Check the count of edges and vertices in each net, to check for cycles
        for info in netInfo:
            v_count = info["vertex_count"]
            e_count = info["edge_count"]
            extra_edges = e_count + 1 - v_count
            if extra_edges > 1:
                msg += "There are {0} extra connections in a signal net connecting these components: {1}\n".format( extra_edges, self.net_to_string(info["net"]))
            elif extra_edges == 1:
                msg += "There is {0} extra connection in a signal net connecting these components: {1}\n".format( extra_edges, self.net_to_string(info["net"]))
        return msg

    # This is the function that checks connections for shorted components,
    # specifically components with 2 or more pins, will all pins connected
    # to the same network.
    # Returns a non-empty error-message string if any shorted components are found.

    def check_for_shorts(self):
        msg = ""
        # Get map of components to a list of their connector instances
        componentNameToConnectorInstanceMap = {}
        for componentId in self.component_id_to_name_map:
            componentName = self.component_id_to_name_map[ componentId ]
            connectorIdList = [x for x in self.connector_id_to_name_map if self.connector_id_to_name_map[x] == componentName]
            componentNameToConnectorInstanceMap[ componentName ] = connectorIdList
        # Find out what networks 2-pin components use
        for componentName in componentNameToConnectorInstanceMap:
            connectorIdList = componentNameToConnectorInstanceMap[ componentName ]
            if len( connectorIdList ) >= 2:
                componentNets = [self.find_net_with_connector(x) for x in connectorIdList ]
                if len(set(componentNets)) <= 1:
                    msg += "Warning! All the pins of component {0} are electrically shorted together.\n".format(componentName )
        return msg

    def find_net_with_connector(self, connectorId):
        r_val = -1
        for i, nodes in enumerate( self.netList ):
            if connectorId in nodes:
                r_val = i
                break
        return r_val


#==========================================================================================================
#
# Scaffolding to allow local stand-alone testing
#
def scan(dir):
    entries = os.listdir(dir)
    for entry in entries:
        fullname = dir + '/' + entry
        if os.path.isdir(fullname):
            scan(fullname)
        else:
            if entry.endswith(".adm"):
                print( "About to check '" + entry + "'\n")
                myNet = NetList( fullname )
                # msg = myNet.check_for_cycles()
                msg = myNet.check_for_shorts()
                print msg
                print( "Done checking '" + entry + "'\n")
    return

# A main program for standalone testing during development.
# HMF 17-JUL-2015
def standAlone():
    scan( "C:\\Users\\Henry\\repos\\morph-subcircuits")
    print( "=== Done. ===")


#start the main program when run as a module
if __name__ == "__main__":
    sys.exit(standAlone())
