# High Current, High Efficiency Single Inductor Buck-Boost Converter
## TPS630250YFFR
### Semiconductors and Actives › Power Management › Voltage Regulators 
***

#### Description

The TPS63025x are high efficiency, low quiescent current buck-boost converters suitable for application where the input voltage is higher or lower than the output. Output currents can go as high as 2 A in boost mode and as high as 4 A in buck mode. The maximum average current in the switches is limited to a typical value of 4 A. The TPS63025x regulates the output voltage over the complete input voltage range by automatically switching between buck or boost moode depending on the input voltage, ensuring a seamless transition between modes. The buck-boost converter is based on a fixed frequency, pulse-width-modulation (PWM) controller using synchronous rectification to obtain highest efficiency. At low load currents, the converter enters Power Save Mode to maintain high efficiency over the complete load current range. There is a PFM/PWM pin that allows the user to choose between automatic PFM/PWM mode operation and forced PWM operation. During PWM mode, a fixed frequency of typically 2.5 MHz is used. The output voltage is programmable using an external resistor divider, or is fixed internally on the chip. The converter can be disabled to minimize battery drain. During shutdown, the load is disconnected from the battery. The device is packaged in a 20-pin WCSP package measuring 1.766 mm x 2.086 mm and  14-pin HotRod package measuring 2.5 mm x 3 mm.

### Connectors 
- ***VOUT* [AnalogSignal]:** Buck-Boost converter output.
- ***FB* [AnalogSignal]:** Voltage feedback of adjustable version, must be connected to VOUT on fixed output voltage version.
- ***L2* [AnalogSignal]:** Connection for Inductor.
- ***PFM/PWM* [DigitalClock]:** Set low for PFM mode, set high for forced PWM mode. Must not be left floating.
- ***L1* [AnalogSignal]:** Connection for Inductor.
- ***EN* [AnalogSignal]:** Enable input. Set high to enable and low to disable. Must not be left floating.
- ***VIN* [SupplySingle]:** Supply voltage for power stage.
- ***VINA* [SupplySingle]:** Supply voltage for control stage.
