# 1 A Synchronous Buck Regulator
## FAN53611UC123X
### Semiconductors and Actives › Power Management › Voltage Regulators 
***

#### Description

The FAN53601/11 is a 6 MHz, step-down switching voltage regulator, available in 600 mA or 1 A options, that delivers a fixed output from an input voltage supply of 2.3 V to 5.5 V. Using a proprietary architecture with synchronous rectification, the FAN53601/11 is capable of delivering a peak efficiency of 92% while maintaining efficiency over 80% at load currents as low as 1 mA.

The regulator operates at a nominal fixed frequency of 6 MHz, which reduces the value of the external components to as low as 470 nH for the output inductor and 4.7 μF for the output capacitor. In addition, the Pulse Width Modulation (PWM) modulator can be synchronized to an external frequency source.

At moderate and light loads, Pulse Frequency Modulation (PFM) is used to operate the device in Power-Save Mode with a typical quiescent current of 24 μA. Even with such a low quiescent current, the part exhibits excellent transient response during large load swings. At higher loads, the system automatically switches to fixed-frequency control, operating at 6 MHz. In Shutdown Mode, the supply current drops below 1 μA, reducing power consumption. For applications that require minimum ripple or fixed frequency, PFM Mode can be disabled using the MODE pin.

The FAN53601/11 is available in 6-bump, 0.4 mm pitch, Wafer-Level Chip-Scale Package (WLCSP).

### Connectors 
- ***MODE* [DigitalSignal]:** MODE. Logic 1 on this pin forces the IC to stay in PWM Mode. A logic 0 allows the IC to automatically switch to PFM during light loads. The regulator also synchronizes its switching frequency to four times the frequency provided on this pin. Do not leave this pin floating. 
- ***SW* [AnalogSignal]:** Switching Node. Connect to output inductor.
- ***FB* [AnalogSignal]:** Feedback/VOUT. Connect to output voltage.
- ***EN* [DigitalSignal]:** Enable. The device is in Shutdown Mode when voltage to this pin is < 0.4 V and enabled when > 1.2 V. Do not leave this pin floating.
