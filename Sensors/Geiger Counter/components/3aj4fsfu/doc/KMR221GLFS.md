KMR221GLFS
===
###### Microminiature SMT Top Actuated, Gull Wing Terminal, Tactile Switch

###### [Datasheet](www.ck-components.com/14414/kmr2_9aug12.pdf/)

## Description
Push-to-make Switch with Ground capability

## Connectors
- ***A* [Std_Pin]:** Switch Terminal
- ***L* [Std_Pin]:** Switch Terminal 

## Design
Activating switch electrically connects A to L

### Component Relationship

### Design Decisions

## Metadata
### Similar Components

### History of Changes