# Low Capacitance Bidirectional ESD Protection Diodes
## PESD5V0S1BB
### Semiconductors and Actives › Discretes › Diodes 
***

#### Description

Low capacitance ElectroStatic Discharge (ESD) protection diodes in ultra small SMD plastic packages designed to protect one signal line from the damage caused by ESD and other transients.

### Connectors 
- ***C1* [AnalogSignal]:** Cathode 1. 
- ***C2* [AnalogSignal]:** Cathode 2. 

