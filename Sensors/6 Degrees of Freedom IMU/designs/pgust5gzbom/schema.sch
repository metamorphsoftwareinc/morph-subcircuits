<?xml version="1.0"?>
<eagle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="6.5.0" xmlns="eagle">
  <compatibility />
  <drawing>
    <settings>
      <setting alwaysvectorfont="no" />
      <setting />
    </settings>
    <grid distance="0.01" unitdist="inch" unit="inch" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch" />
    <layers>
      <layer number="1" name="Top" color="4" fill="1" visible="no" active="no" />
      <layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no" />
      <layer number="17" name="Pads" color="2" fill="1" visible="no" active="no" />
      <layer number="18" name="Vias" color="2" fill="1" visible="no" active="no" />
      <layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no" />
      <layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no" />
      <layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="25" name="tNames" color="7" fill="1" visible="no" active="no" />
      <layer number="26" name="bNames" color="7" fill="1" visible="no" active="no" />
      <layer number="27" name="tValues" color="7" fill="1" visible="no" active="no" />
      <layer number="28" name="bValues" color="7" fill="1" visible="no" active="no" />
      <layer number="29" name="tStop" color="7" fill="3" visible="no" active="no" />
      <layer number="30" name="bStop" color="7" fill="6" visible="no" active="no" />
      <layer number="31" name="tCream" color="7" fill="4" visible="no" active="no" />
      <layer number="32" name="bCream" color="7" fill="5" visible="no" active="no" />
      <layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no" />
      <layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no" />
      <layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no" />
      <layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no" />
      <layer number="37" name="tTest" color="7" fill="1" visible="no" active="no" />
      <layer number="38" name="bTest" color="7" fill="1" visible="no" active="no" />
      <layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no" />
      <layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no" />
      <layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no" />
      <layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no" />
      <layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no" />
      <layer number="44" name="Drills" color="7" fill="1" visible="no" active="no" />
      <layer number="45" name="Holes" color="7" fill="1" visible="no" active="no" />
      <layer number="46" name="Milling" color="3" fill="1" visible="no" active="no" />
      <layer number="47" name="Measures" color="7" fill="1" visible="no" active="no" />
      <layer number="48" name="Document" color="7" fill="1" visible="no" active="no" />
      <layer number="49" name="Reference" color="7" fill="1" visible="no" active="no" />
      <layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="91" name="Nets" color="2" fill="1" />
      <layer number="92" name="Busses" color="1" fill="1" />
      <layer number="93" name="Pins" color="2" fill="1" visible="no" />
      <layer number="94" name="Symbols" color="4" fill="1" />
      <layer number="95" name="Names" color="7" fill="1" />
      <layer number="96" name="Values" color="7" fill="1" />
      <layer number="97" name="Info" color="7" fill="1" />
      <layer number="98" name="Guide" color="6" fill="1" />
    </layers>
    <schematic xrefpart="/%S.%C%R" xreflabel="%F%N/%S.%C%R">
      <description />
      <libraries>
        <library name="mlcc">
          <description />
          <packages>
            <package name="C_0603">
              <description>&lt;B&gt; 0603&lt;/B&gt; (1608 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="-0.5334" x2="0.1016" y2="-0.5334" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="0.5334" x2="0.1016" y2="0.5334" width="0.1524" layer="21" />
              <smd name="1" x="-0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <smd name="2" x="0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <text x="-1.6" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="CAP_NP">
              <description>&lt;B&gt;Capacitor&lt;/B&gt; -- non-polarized</description>
              <wire x1="-1.905" y1="-3.175" x2="0" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="0" y1="-3.175" x2="1.905" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="-1.905" y1="-4.445" x2="0" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-4.445" x2="1.905" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-2.54" x2="0" y2="-3.175" width="0.254" layer="94" />
              <wire x1="0" y1="-5.08" x2="0" y2="-4.445" width="0.254" layer="94" />
              <pin name="P$1" x="0" y="0" visible="off" length="short" direction="pas" rot="R270" />
              <pin name="P$2" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90" />
              <text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
              <text x="-5.08" y="-7.62" size="1.778" layer="95" rot="R90">&gt;NAME</text>
              <text x="0.508" y="-2.286" size="1.778" layer="95">1</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="C" name="C_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0603">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="TimsLibrary">
          <description />
          <packages>
            <package name="LGA14L_DOT_INDICATOR">
              <description />
              <circle x="-1.95" y="1.7" radius="0.1" width="0.125" layer="21" />
              <wire x1="-1.5" y1="-1.25" x2="1.5" y2="-1.25" width="0" layer="51" />
              <wire x1="1.5" y1="-1.25" x2="1.5" y2="1.25" width="0" layer="51" />
              <wire x1="1.5" y1="1.25" x2="-1.5" y2="1.25" width="0" layer="51" />
              <wire x1="-1.5" y1="1.25" x2="-1.5" y2="-1.25" width="0" layer="51" />
              <wire x1="-1.625" y1="1.375" x2="1.625" y2="1.375" width="0.25" layer="21" />
              <wire x1="1.625" y1="1.375" x2="1.625" y2="-1.375" width="0.25" layer="21" />
              <wire x1="1.625" y1="-1.375" x2="-1.625" y2="-1.375" width="0.25" layer="21" />
              <wire x1="-1.625" y1="-1.375" x2="-1.625" y2="1.375" width="0.25" layer="21" />
              <wire x1="-0.25" y1="1.15" x2="-0.25" y2="0.42" width="0.01" layer="41" />
              <wire x1="0.25" y1="1.15" x2="0.25" y2="0.42" width="0.01" layer="41" />
              <wire x1="-0.25" y1="-0.42" x2="-0.25" y2="-1.15" width="0.01" layer="41" />
              <wire x1="0.25" y1="-0.42" x2="0.25" y2="-1.15" width="0.01" layer="41" />
              <wire x1="-1.4" y1="0.5" x2="-0.67" y2="0.5" width="0.01" layer="41" />
              <wire x1="-1.4" y1="0" x2="-0.67" y2="0" width="0.01" layer="41" />
              <wire x1="-1.4" y1="-0.5" x2="-0.67" y2="-0.5" width="0.01" layer="41" />
              <wire x1="0.67" y1="0.5" x2="1.4" y2="0.5" width="0.01" layer="41" />
              <wire x1="0.67" y1="0" x2="1.4" y2="0" width="0.01" layer="41" />
              <wire x1="0.67" y1="-0.5" x2="1.4" y2="-0.5" width="0.01" layer="41" />
              <wire x1="-1.925" y1="1.7" x2="-1.975" y2="1.7" width="0.25" layer="21" />
              <rectangle x1="-0.83" y1="-0.58" x2="0.83" y2="0.58" layer="41" />
              <rectangle x1="-1.5" y1="0.98" x2="-0.74" y2="1.25" layer="41" />
              <rectangle x1="0.74" y1="0.98" x2="1.5" y2="1.25" layer="41" />
              <rectangle x1="0.74" y1="-1.25" x2="1.5" y2="-0.98" layer="41" />
              <rectangle x1="-1.5" y1="-1.25" x2="-0.74" y2="-0.98" layer="41" />
              <rectangle x1="-0.83" y1="0.58" x2="-0.74" y2="0.98" layer="41" />
              <rectangle x1="0.74" y1="0.58" x2="0.83" y2="0.98" layer="41" />
              <rectangle x1="0.74" y1="-0.98" x2="0.83" y2="-0.58" layer="41" />
              <rectangle x1="-0.83" y1="-0.98" x2="-0.74" y2="-0.58" layer="41" />
              <smd name="1" x="-1.175" y="0.75" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="2" x="-1.175" y="0.25" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="3" x="-1.175" y="-0.25" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="4" x="-1.175" y="-0.75" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="5" x="-0.5" y="-0.925" dx="0.45" dy="0.25" layer="1" rot="R90" />
              <smd name="6" x="0" y="-0.925" dx="0.45" dy="0.25" layer="1" rot="R90" />
              <smd name="7" x="0.5" y="-0.925" dx="0.45" dy="0.25" layer="1" rot="R90" />
              <smd name="8" x="1.175" y="-0.75" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="9" x="1.175" y="-0.25" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="10" x="1.175" y="0.25" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="11" x="1.175" y="0.75" dx="0.45" dy="0.25" layer="1" rot="R180" />
              <smd name="12" x="0.5" y="0.925" dx="0.45" dy="0.25" layer="1" rot="R90" />
              <smd name="13" x="0" y="0.925" dx="0.45" dy="0.25" layer="1" rot="R90" />
              <smd name="14" x="-0.5" y="0.925" dx="0.45" dy="0.25" layer="1" rot="R90" />
              <text x="-2.5" y="1.8" size="1" layer="27" font="vector" ratio="15">&gt;Value</text>
              <text x="-2.5" y="-2.8" size="1" layer="27" font="vector" ratio="15">&gt;Name</text>
            </package>
          </packages>
          <symbols>
            <symbol name="LSM6DS3">
              <description />
              <wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94" />
              <pin name="SDO/SA0" x="-12.7" y="7.62" length="short" />
              <pin name="SDX" x="-12.7" y="5.08" length="short" />
              <pin name="SCX" x="-12.7" y="2.54" length="short" />
              <pin name="INT1" x="-12.7" y="0" length="short" />
              <pin name="VDDIO" x="-12.7" y="-2.54" length="short" />
              <pin name="GND" x="-12.7" y="-5.08" length="short" />
              <pin name="GND2" x="-12.7" y="-7.62" length="short" />
              <pin name="VDD" x="12.7" y="-7.62" length="short" rot="R180" />
              <pin name="INT2" x="12.7" y="-5.08" length="short" rot="R180" />
              <pin name="OCS" x="12.7" y="-2.54" length="short" rot="R180" />
              <pin name="NC" x="12.7" y="0" length="short" rot="R180" />
              <pin name="CS" x="12.7" y="2.54" length="short" rot="R180" />
              <pin name="SCL" x="12.7" y="5.08" length="short" rot="R180" />
              <pin name="SDA" x="12.7" y="7.62" length="short" rot="R180" />
              <text x="-8.89" y="13.97" size="1.778" layer="95">&gt;NAME</text>
              <text x="-8.89" y="11.43" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset name="LSM6DS3">
              <description />
              <gates>
                <gate name="G$1" symbol="LSM6DS3" x="0" y="0" />
              </gates>
              <devices>
                <device package="LGA14L_DOT_INDICATOR">
                  <connects>
                    <connect gate="G$1" pin="CS" pad="12" />
                    <connect gate="G$1" pin="GND" pad="6" />
                    <connect gate="G$1" pin="GND2" pad="7" />
                    <connect gate="G$1" pin="INT1" pad="4" />
                    <connect gate="G$1" pin="INT2" pad="9" />
                    <connect gate="G$1" pin="NC" pad="11" />
                    <connect gate="G$1" pin="OCS" pad="10" />
                    <connect gate="G$1" pin="SCL" pad="13" />
                    <connect gate="G$1" pin="SCX" pad="3" />
                    <connect gate="G$1" pin="SDA" pad="14" />
                    <connect gate="G$1" pin="SDO/SA0" pad="1" />
                    <connect gate="G$1" pin="SDX" pad="2" />
                    <connect gate="G$1" pin="VDD" pad="8" />
                    <connect gate="G$1" pin="VDDIO" pad="5" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="resistor">
          <description />
          <packages>
            <package name="R_0603">
              <description>&lt;B&gt;
0603
&lt;/B&gt; SMT inch-code chip resistor package&lt;P&gt;
Derived from dimensions and tolerances
in the &lt;B&gt; &lt;A HREF="http://www.samsungsem.com/global/support/library/product-catalog/__icsFiles/afieldfile/2015/01/12/CHIP_RESISTOR_150112_1.pdf"&gt;Samsung Thick Film Chip Resistor Catalog&lt;/A&gt;&lt;/B&gt;
dated December 2014,
for 
general-purpose chip resistor
reflow soldering.</description>
              <wire x1="-0.1" y1="0.3" x2="0.1" y2="0.3" width="0.1524" layer="21" />
              <wire x1="-0.1" y1="-0.3" x2="0.1" y2="-0.3" width="0.1524" layer="21" />
              <smd name="P$1" x="-0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <smd name="P$2" x="0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <text x="-1.3" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
            </package>
          </packages>
          <symbols>
            <symbol name="R">
              <description>&lt;B&gt;Resistor&lt;/B&gt;</description>
              <wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94" />
              <wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94" />
              <wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94" />
              <pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" />
              <pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180" />
              <text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
              <text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="R" name="RESISTOR_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="R" x="0" y="0" />
              </gates>
              <devices>
                <device package="R_0603">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
      </libraries>
      <attributes />
      <variantdefs />
      <classes>
        <class number="0" name="default" />
      </classes>
      <parts>
        <part device="" value="CLLC1AX7S0G104M050AC" name="C3" library="mlcc" deviceset="C_0603" />
        <part device="" value="CLLC1AX7S0G104M050AC" name="C4" library="mlcc" deviceset="C_0603" />
        <part device="" value="LSM6DS3" name="LSM6DS3" library="TimsLibrary" deviceset="LSM6DS3" />
        <part device="" value="CR0603-FX-1002ELF" name="R4" library="resistor" deviceset="RESISTOR_0603" />
      </parts>
      <sheets>
        <sheet>
          <description />
          <plain />
          <instances>
            <instance y="30.48" part="C3" gate="G$1" x="96.52" />
            <instance y="38.10" part="C4" gate="G$1" x="38.10" />
            <instance y="35.56" part="LSM6DS3" gate="G$1" x="58.42" />
            <instance y="12.70" part="R4" gate="G$1" x="83.82" />
          </instances>
          <busses />
          <nets>
            <net name="N$0">
              <segment>
                <wire x1="96.52" y1="22.86" x2="96.52" y2="20.32" width="0.3" layer="91" />
                <label x="96.52" y="20.32" size="1.27" layer="95" />
                <pinref part="C3" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="45.72" y1="30.48" x2="43.18" y2="30.48" width="0.3" layer="91" />
                <label x="43.18" y="30.48" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="GND" />
              </segment>
              <segment>
                <wire x1="45.72" y1="27.94" x2="43.18" y2="27.94" width="0.3" layer="91" />
                <label x="43.18" y="27.94" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="GND2" />
              </segment>
              <segment>
                <wire x1="38.10" y1="30.48" x2="38.10" y2="27.94" width="0.3" layer="91" />
                <label x="38.10" y="27.94" size="1.27" layer="95" />
                <pinref part="C4" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="45.72" y1="40.64" x2="43.18" y2="40.64" width="0.3" layer="91" />
                <label x="43.18" y="40.64" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="SDX" />
              </segment>
              <segment>
                <wire x1="45.72" y1="38.10" x2="43.18" y2="38.10" width="0.3" layer="91" />
                <label x="43.18" y="38.10" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="SCX" />
              </segment>
            </net>
            <net name="N$1">
              <segment>
                <wire x1="96.52" y1="30.48" x2="96.52" y2="33.02" width="0.3" layer="91" />
                <label x="96.52" y="33.02" size="1.27" layer="95" />
                <pinref part="C3" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="78.74" y1="12.70" x2="76.20" y2="12.70" width="0.3" layer="91" />
                <label x="76.20" y="12.70" size="1.27" layer="95" />
                <pinref part="R4" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="45.72" y1="43.18" x2="43.18" y2="43.18" width="0.3" layer="91" />
                <label x="43.18" y="43.18" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="SDO/SA0" />
              </segment>
              <segment>
                <wire x1="45.72" y1="33.02" x2="43.18" y2="33.02" width="0.3" layer="91" />
                <label x="43.18" y="33.02" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="VDDIO" />
              </segment>
              <segment>
                <wire x1="38.10" y1="38.10" x2="38.10" y2="40.64" width="0.3" layer="91" />
                <label x="38.10" y="40.64" size="1.27" layer="95" />
                <pinref part="C4" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="71.12" y1="27.94" x2="73.66" y2="27.94" width="0.3" layer="91" />
                <label x="73.66" y="27.94" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="VDD" />
              </segment>
            </net>
            <net name="N$2">
              <segment>
                <wire x1="71.12" y1="38.10" x2="73.66" y2="38.10" width="0.3" layer="91" />
                <label x="73.66" y="38.10" size="1.27" layer="95" />
                <pinref part="LSM6DS3" gate="G$1" pin="CS" />
              </segment>
              <segment>
                <wire x1="88.90" y1="12.70" x2="91.44" y2="12.70" width="0.3" layer="91" />
                <label x="91.44" y="12.70" size="1.27" layer="95" />
                <pinref part="R4" gate="G$1" pin="2" />
              </segment>
            </net>
          </nets>
        </sheet>
      </sheets>
      <errors />
    </schematic>
  </drawing>
  <compatibility />
</eagle>
