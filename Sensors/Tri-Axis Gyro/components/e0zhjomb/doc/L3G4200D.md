# MEMS motion sensor: ultra-stable three-axis digital output gyroscope
## L3G4200D
### Semiconductors and Actives > Sensors > Gyroscopes
***

###Summary
GYROSCOPE MEMS 3-AXIS 16LGA

####General Description
The L3G4200D is a low-power three-axis angular rate sensor able to provide unprecedented stablility of zero rate level and sensitivity over temperature and time. It includes a sensing element and an IC interface capable of providing the measured angular rate to the external world through a digital interface (I2C/SPI).

The sensing element is manufactured using a dedicated micro-machining process developed by STMicroelectronics to produce inertial sensors and actuators on silicon wafers.

The IC interface is manufactured using a CMOS process that allows a high level of integration to design a dedicated circuit which is trimmed to better match the sensing element characteristics. 

The L3G4200D has a full scale of ±250/±500/±2000 dps and is capable of measuring rates with a user-selectable bandwidth.

The L3G4200D is available in a plastic land grid array (LGA) package and can operate within a temperature range of -40 °C to +85 °C.

###Connectors
- ***VDD* [PwrGnd_TwoPort]:** Power supply.
- ***VDDIO* [PwrGnd_TwoPort]:** Power supply for I/O pins.
- ***I2C* [I2C]:** I2C serial interface.
- ***SPI* [SPI]:** SPI serial interface.
- ***SDO*, *SA0* [DigitalSignal]:** SPI serial data output (SDO), I2C least significant bit of the device address (SA0).
- ***CS* [DigitalSignal]:** SPI enable. I2C/SPI mode selection (1: SPI idle mode/I2C connection enabled; 0: SPI communication mode/I2C disabled).
- ***INT1* [DigitalSignal]:** Programmable interrupt.
- ***INT2/DRDY* [DigitalSignal]:** Data ready/FIFO interrupt.
- ***PLL* [AnalogSignal]:** Phase-locked loop filter.
