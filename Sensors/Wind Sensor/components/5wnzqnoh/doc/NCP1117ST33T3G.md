# Low-Dropout Positive Fixed and Adjustable Voltage Regulators
## NCP1117ST33T3G
### Semiconductors and Actives › Power Management › Voltage Regulators 
***

#### Description

The NCP1117 series are low dropout positive voltage regulators that are capable of providing an output current that is in excess of 1.0 A with a maximum dropout voltage of 1.2 V at 800 mA over temperature. This series contains nine fixed output voltages of 1.5 , 1.8 V, 1.9 V, 2.0 V, 2.5 V, 2.85 V, 3.3 V, 5.0 V, and 12 V that have no minimum load requirement to maintain regulation. Also included is an adjustable output version that can be programmed from 1.25 C to 18.8 V with two external resistors. On chip trimming adjusts the reference/output voltage to within ±1.0% accuracy. Internal protection features consist of output current limiting, safe operating area compensation, and thermal shutdown. The NCP1117 series can operate with up to 20 V input. Devices are available in SOT-223 and DPAK packages.

### Connectors 
- ***Input* []:** Input. 
- ***Output* []:** Output. 

