# Light to Digital with I2C
## TSL2561
### Optoelectronics > Detectors > 
***

### Summary
Light to Ditial 6-LEAD CS

#### General Description
The TSL2560 and TSL2561 are light-to-digital converters that transform light intensity to a digital signal output capable of direct I2C (TSL2561) or SMBus (TSL2560) interface. Each device combines one broadband photodiode (visible plus infrared) and one infrared-responding photodiode
on a single CMOS integrated circuit capable of providing a near-photopic response over an effective 20-bit dynamic range (16-bit resolution).
Two integrating ADCs convert the photodiode currents to a digital output that represents the irradiance measured on each channel. This digital
output can be input to a microprocessor where illuminance (ambient light level) in lux is derived using an empirical formula to approximate the
human eye response. The TSL2560 device permits an SMB-Alert style interrupt, and the TSL2561 device supports a traditional level style
interrupt that remains asserted until the firmware clears it.

### Connectors 
- ***VDD* [PwrGnd_TwoPort]:** Power Supply
- ***SCL* [I2C]:** I2C serial bus clock input
- ***SDA* [I2C]:** I2C serial bus data
- ***INT* [DigitalSignal]:** Level or SMB Alert interrupt --open drain
- ***ADDR* [DigitalSignal]:** SMBus device select -- three-state

