<eagle version="6.5.0" xmlns="eagle">
  <drawing>
    <library>
      <packages>
        <package name="1X06-SIP_LOCK" xmlns="eagle">
          <description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
          <wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.15239999999999998" layer="21" />
          <wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.15239999999999998" layer="21" />
          <wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.15239999999999998" layer="21" />
          <wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.15239999999999998" layer="21" />
          <wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.15239999999999998" layer="21" />
          <wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.15239999999999998" layer="21" />
          <wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.15239999999999998" layer="21" />
          <wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.15239999999999998" layer="21" />
          <wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.15239999999999998" layer="21" />
          <pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" />
          <pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796" />
          <pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" />
          <pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796" />
          <pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" />
          <pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796" />
          <text x="-1.27" y="1.524" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
          <rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51" />
          <rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51" />
          <rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51" />
          <rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" />
          <rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51" />
          <rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51" />
        </package>
      </packages>
      <symbols>
        <symbol name="M06">
          <wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94" />
          <wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94" />
          <wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94" />
          <wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94" />
          <wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94" />
          <wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94" />
          <wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94" />
          <wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94" />
          <wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94" />
          <wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94" />
          <text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
          <text x="-5.08" y="10.922" size="1.778" layer="95">&gt;NAME</text>
          <pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
          <pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
          <pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
          <pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
          <pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
          <pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
        </symbol>
      </symbols>
      <devicesets>
        <deviceset name="M06" prefix="JP" uservalue="yes">
          <gates>
            <gate name="G$1" symbol="M06" x="-2.54" y="0" />
          </gates>
          <devices>
            <device name="SIP_LOCK" package="1X06-SIP_LOCK" xmlns="eagle">
              <connects>
                <connect gate="G$1" pin="1" pad="1" />
                <connect gate="G$1" pin="2" pad="2" />
                <connect gate="G$1" pin="3" pad="3" />
                <connect gate="G$1" pin="4" pad="4" />
                <connect gate="G$1" pin="5" pad="5" />
                <connect gate="G$1" pin="6" pad="6" />
              </connects>
              <technologies>
                <technology name="" />
              </technologies>
            </device>
          </devices>
        </deviceset>
      </devicesets>
    </library>
  </drawing>
</eagle>