# PART NAME
## ECS-.327-12.5-13FLX-C
### Passive Components > Crystals/Resonators/Oscillators > Crystals
***

### Summary
CRYSTAL 32.768KHZ 12.5PF SMD

#### General Description
SMD Tuning Fork Crystal

### Connectors 
- ***1* [StdPin]:** Terminal 1 
- ***2* [StdPin]:** Terminal 2

