#!/bin/bash -x

set -e

export PATH="$PATH":/c/python27
which python

export PYTHONUSERBASE=~/.morph-subcircuits-python
echo $PYTHONUSERBASE
python -m pip install --user -r "zExtra Stuff/scripts/testSubcircuits/requirements.txt"
python "zExtra Stuff/scripts/testSubcircuits/findSubcircuits.py" "$@"
